import Vue from 'vue'
import App from './App.vue'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import InputMask from 'vue-input-mask';
import AOS from 'aos'
import 'aos/dist/aos.css'
import vuetify from '@/plugins/vuetify';

Vue.config.productionTip = false

Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.component('input-mask', InputMask)


new Vue({
  created () {
    AOS.init()
  },
  vuetify,
  render: h => h(App),
}).$mount('#app')


